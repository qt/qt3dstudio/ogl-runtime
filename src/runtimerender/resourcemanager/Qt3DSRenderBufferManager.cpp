/****************************************************************************
**
** Copyright (C) 2008-2012 NVIDIA Corporation.
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of Qt 3D Studio.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/
#ifdef _WIN32
#pragma warning(disable : 4201) // nonstandard extension used : nameless struct/union
#endif
#include "Qt3DSRender.h"
#include "Qt3DSRenderBufferManager.h"
#include "EASTL/string.h"
#include "foundation/Qt3DSAllocator.h"
#include "render/Qt3DSRenderContext.h"
#include "foundation/Qt3DSAtomic.h"
#include "EASTL/hash_map.h"
#include "foundation/FileTools.h"
#include "Qt3DSImportMesh.h"
#include "Qt3DSRenderMesh.h"
#include "foundation/Qt3DSAllocatorCallback.h"
#include "Qt3DSRenderLoadedTexture.h"
#include "foundation/Qt3DSFoundation.h"
#include "Qt3DSRenderInputStreamFactory.h"
#include "Qt3DSRenderImageScaler.h"
#include "Qt3DSRenderImage.h"
#include "Qt3DSTextRenderer.h"
#include "foundation/Qt3DSPerfTimer.h"
#include "foundation/Qt3DSMutex.h"
#include "Qt3DSRenderPrefilterTexture.h"
#include <QtCore/qdir.h>

using namespace qt3ds::render;

namespace qt3ds {
namespace render {

static const QString QT3DSTUDIO_TAG = QStringLiteral("qt3dstudio");

static QFileInfo matchCaseInsensitiveFile(const QString& file)
{
    QStringList searchDirectories = QDir::searchPaths(QT3DSTUDIO_TAG);
    std::reverse(searchDirectories.begin(), searchDirectories.end());

    const auto searchFromPaths = [searchDirectories](QString file) -> QFileInfo {
        QFileInfo fileInfo(file);
        if (fileInfo.exists())
            return fileInfo;

        for (const auto &directoryPath : searchDirectories) {
            auto path = QDir::cleanPath(directoryPath + '/' + file);
            QFileInfo info(path);
            if (info.exists())
                return info;
        }
        return fileInfo;
    };

    QFileInfo info = searchFromPaths(file);
    if (info.exists())
        return info;

    QString searchPath = file;

    // Trying relative to search directories.
    // First remove any ../ and ./ from the path
    if (searchPath.startsWith(QLatin1String("../")))
        searchPath = searchPath.right(searchPath.length() - 3);
    else if (searchPath.startsWith(QLatin1String("./")))
        searchPath = searchPath.right(searchPath.length() - 2);

    int loops = 0;
    while (++loops <= 4) {
        info = searchFromPaths(searchPath);
        if (info.exists())
            return info;
        searchPath.prepend(QLatin1String("../"));
    }

    return info;
}

static bool checkFileExists(const QString &tryFile, bool relative, QString &outFile)
{
    QFileInfo info;
    if (relative)
        info = matchCaseInsensitiveFile(tryFile);
    else
        info.setFile(tryFile);
    if (info.exists()) {
        outFile = info.filePath();
        return true;
    }
    if (!relative) {
        // Some textures, for example environment maps for custom materials,
        // have absolute path at this point. It points to the wrong place with
        // the new project structure, so we need to split it up and construct
        // the new absolute path here.
        QString wholePath = tryFile;
        QStringList splitPath = wholePath.split(QLatin1String("../"));
        if (splitPath.size() > 1) {
            QString searchPath = splitPath.first() + splitPath.last();
            int loops = 0;
            while (++loops <= 3) {
                info.setFile(searchPath);
                if (info.exists()) {
                    outFile = info.filePath();
                    return true;
                }
                searchPath = splitPath.at(0);
                for (int i = 0; i < loops; i++)
                    searchPath.append(QLatin1String("../"));
                searchPath.append(splitPath.last());
            }
        }
    }
    return false;
}

// Locate existing file by adding a supported suffix to localFile.
static bool existingImageFileForPath(const QString &localFile, bool relative, bool preferKTX,
                                     QString &outPath)
{
    // Do nothing if given filepath exists without suffix
    QFileInfo fi(localFile);
    if (fi.exists()) {
        outPath = localFile;
        return true;
    }

    // Lists of supported image formats in preferred-first order.
    const QStringList compressedFormats {"ktx", "astc", "dds"};
    const QStringList nonCompressedFormats {"hdr", "png", "jpg", "jpeg", "gif"};

    // Depending on preferKTX, check compressed formats before or after non-compressed ones.
    QStringList supportedFormats = preferKTX ? compressedFormats + nonCompressedFormats
                                             :  nonCompressedFormats + compressedFormats;

    // Check first if file exists from resources as that
    // is common and optimal for integrity case.
    for (const QString &suffix : supportedFormats) {
        QString tryFile = ":/" + localFile + "." + suffix;
        if (checkFileExists(tryFile, relative, outPath))
            return true;
    }
    // If not found, check still file path as-is
    for (const QString &suffix : supportedFormats) {
        QString tryFile = localFile + "." + suffix;
        if (checkFileExists(tryFile, relative, outPath))
            return true;
    }

    outPath = localFile;
    return false;
}

// Return resolved image path for unresolved source path.
// Handle preferKtx, search paths, empty suffixes, schemas etc.
QString IBufferManager::resolveImagePathInternal(const QString &sourcePath, bool preferKtx)
{
    QString path = sourcePath;
    QUrl urlPath(path);

    // Do nothing for image providers
    if (urlPath.scheme() == QLatin1String("image"))
        return sourcePath;

    // Normalize the path
    path = CFileTools::NormalizePathForQtUsage(path);
    urlPath.setUrl(path);

    const bool hasSuffix = urlPath.fileName().contains(QLatin1Char('.'));
    const bool relative = urlPath.isRelative();

    // Find an image file corresponding to only filename without suffix
    if (!hasSuffix) {
        QString outPath;
        if (!existingImageFileForPath(path, relative, preferKtx, outPath))
            return {};

        return outPath;
    }

    // First check ktx files if preferKtx is set
    QString result;
    if (preferKtx) {
        QString ktxSource = path;
        const bool originalIsKtx = path.endsWith(QLatin1String("ktx"), Qt::CaseInsensitive);
        if (!originalIsKtx) {
            const int index = ktxSource.lastIndexOf(QLatin1Char('.'));
            ktxSource = ktxSource.left(index);
            ktxSource.append(QLatin1String(".ktx"));
        }
        if (checkFileExists(ktxSource, relative, result))
            return result;
        if (originalIsKtx)
            return {};
    }

    if (checkFileExists(path, relative, result))
        return result;

    return {};
}

static QStringList s_failedPaths = {};

QString IBufferManager::resolveImagePath(const QString &sourcePath, bool preferKtx)
{
    if (sourcePath.isEmpty())
        return {};

    if (!s_failedPaths.contains(sourcePath)) {
        QString path = resolveImagePathInternal(sourcePath, preferKtx);
        if (!path.isEmpty())
            return path;

        qCWarning(WARNING, "Failed to resolve path %s", qPrintable(sourcePath));
        s_failedPaths.append(sourcePath);
    }
    return {};
}

QSet<QString> IBufferManager::resolveImageSet(const QSet<QString> &set, bool preferKtx)
{
    QSet<QString> ret;
    for (auto &sourcePath : set) {
        auto resolved = resolveImagePath(sourcePath, preferKtx);
        if (!resolved.isEmpty())
            ret.insert(resolved);
    }
    return ret;
}

QVector<CRegisteredString> IBufferManager::resolveSourcePaths(
        IStringTable &strTable, const QVector<CRegisteredString> &sourcePaths, bool preferKtx)
{
    QVector<CRegisteredString> ret;
    for (int i = 0; i < sourcePaths.size(); i++) {
        QString path = QString::fromLatin1(sourcePaths[i].c_str());
        auto resolved = strTable.RegisterStr(IBufferManager::resolveImagePath(path, preferKtx));
        if (resolved.IsValid())
            ret.append(resolved);
    }
    return ret;
}

}
}

namespace {

using eastl::hash;
using eastl::pair;
using eastl::make_pair;
typedef eastl::basic_string<char8_t, ForwardingAllocator> TStr;
struct StrHasher
{
    size_t operator()(const TStr &str) const
    {
        return hash<const char8_t *>()((const char8_t *)str.c_str());
    }
};

struct StrEq
{
    bool operator()(const TStr &lhs, const TStr &rhs) const { return lhs == rhs; }
};

struct SImageEntry : public SImageTextureData
{
    bool m_Loaded;
    SImageEntry()
        : SImageTextureData(), m_Loaded(false)
    {
    }
    SImageEntry(const SImageEntry &entry)
        : SImageTextureData(entry), m_Loaded(entry.m_Loaded)
    {

    }
};

struct SPrimitiveEntry
{
    // Name of the primitive as it will be in the UIP file
    CRegisteredString m_PrimitiveName;
    // Name of the primitive file on the filesystem
    CRegisteredString m_FileName;
};

struct SBufferManager : public IBufferManager
{
    typedef eastl::hash_set<CRegisteredString, eastl::hash<CRegisteredString>,
                            eastl::equal_to<CRegisteredString>, ForwardingAllocator>
        TStringSet;
    typedef QHash<QString, SImageEntry> TImageMap;
    typedef nvhash_map<CRegisteredString, SRenderMesh *> TMeshMap;
    typedef QMap<QString, QString> TAliasImageMap;

    NVScopedRefCounted<NVRenderContext> m_Context;
    NVScopedRefCounted<IStringTable> m_StrTable;
    NVScopedRefCounted<IInputStreamFactory> m_InputStreamFactory;
    IPerfTimer &m_PerfTimer;
    volatile QT3DSI32 mRefCount;
    TStr m_PathBuilder;
    TImageMap m_ImageMap;
    Mutex m_LoadedImageSetMutex;
    QSet<QString> m_LoadedImageSet;
    QMap<QString, QString> m_AliasImageMap;
    TMeshMap m_MeshMap;
    SPrimitiveEntry m_PrimitiveNames[5];
    nvvector<qt3ds::render::NVRenderVertexBufferEntry> m_EntryBuffer;
    bool m_GPUSupportsCompressedTextures;
    bool m_reloadableResources;
    QHash<QString, QSharedPointer<QQmlImageProviderBase> > m_imageProviders;

    QHash<QString, ReloadableTexturePtr> m_reloadableTextures;
    ReloadableTexturePtr m_nullTexture;

    bool m_allAstcPremultiplied = false;

    static const char8_t *GetPrimitivesDirectory() { return "res//primitives"; }

    SBufferManager(NVRenderContext &ctx, IStringTable &strTable,
                   IInputStreamFactory &inInputStreamFactory, IPerfTimer &inTimer)
        : m_Context(ctx)
        , m_StrTable(strTable)
        , m_InputStreamFactory(inInputStreamFactory)
        , m_PerfTimer(inTimer)
        , mRefCount(0)
        , m_PathBuilder(ForwardingAllocator(ctx.GetAllocator(), "SBufferManager::m_PathBuilder"))
        , m_LoadedImageSetMutex(ctx.GetAllocator())
        , m_MeshMap(ctx.GetAllocator(), "SBufferManager::m_MeshMap")
        , m_EntryBuffer(ctx.GetAllocator(), "SBufferManager::m_EntryBuffer")
        , m_GPUSupportsCompressedTextures(ctx.AreCompressedTexturesSupported())
        , m_reloadableResources(false)
    {
#ifdef QT3DS_ALL_ASTC_PREMULTIPLIED
        m_allAstcPremultiplied = true;
#else
        m_allAstcPremultiplied = qEnvironmentVariableIntValue("QT3DS_ALL_ASTC_PREMULTIPLIED");
#endif
    }
    virtual ~SBufferManager() { Clear(); }

    QT3DS_IMPLEMENT_REF_COUNT_ADDREF_RELEASE(m_Context->GetAllocator())

    void checkAlwaysPremultiplied(const QString &resolvedPath,
                                  SImageTextureFlags &textureFlags) override
    {
        if (m_allAstcPremultiplied) {
            if (resolvedPath.endsWith(QStringLiteral(".astc")))
                textureFlags.SetPreMultiplied(true);
        }
    }

    void SetImageHasTransparency(const QString &resolvedPath, bool inHasTransparency,
                                 bool hasOpaque) override
    {
        if (!m_ImageMap.contains(resolvedPath))
            m_ImageMap.insert(resolvedPath, SImageEntry());
        SImageEntry &entry = m_ImageMap[resolvedPath];
        entry.m_TextureFlags.SetHasTransparency(inHasTransparency);
        entry.m_TextureFlags.setHasOpaquePixels(hasOpaque);
        checkAlwaysPremultiplied(resolvedPath, entry.m_TextureFlags);
    }

    bool GetImageHasTransparency(const QString &resolvedPath) const override
    {
        TImageMap::const_iterator theIter = m_ImageMap.find(resolvedPath);
        if (theIter != m_ImageMap.end())
            return theIter->m_TextureFlags.HasTransparency();
        return false;
    }

    bool GetImageHasOpaquePixels(const QString &resolvedPath) const override
    {
        TImageMap::const_iterator theIter = m_ImageMap.find(resolvedPath);
        if (theIter != m_ImageMap.end())
            return theIter->m_TextureFlags.HasOpaquePixels();
        return false;
    }

    void SetImageTransparencyToFalseIfNotSet(const QString &resolvedPath) override
    {
        if (!m_ImageMap.contains(resolvedPath)) {
            m_ImageMap.insert(resolvedPath, SImageEntry());
            m_ImageMap[resolvedPath].m_TextureFlags.SetHasTransparency(false);
        }
    }

    void SetInvertImageUVCoords(const QString &resolvedPath, bool inShouldInvertCoords) override
    {
        if (!m_ImageMap.contains(resolvedPath))
            m_ImageMap.insert(resolvedPath, SImageEntry());
        SImageEntry &entry = m_ImageMap[resolvedPath];
        entry.m_TextureFlags.SetInvertUVCoords(inShouldInvertCoords);
    }

    bool IsImageLoaded(const QString &sourcePath) override
    {
        Mutex::ScopedLock __locker(m_LoadedImageSetMutex);
        return m_LoadedImageSet.find(sourcePath) != m_LoadedImageSet.end();
    }

    bool AliasImagePath(const QString &resolvedPath, const QString &inAliasPath,
                        bool inIgnoreIfLoaded) override
    {
        if (resolvedPath.isEmpty() || inAliasPath.isEmpty())
            return false;
        // If the image is loaded then we ignore this call in some cases.
        if (inIgnoreIfLoaded && IsImageLoaded(resolvedPath))
            return false;
        m_AliasImageMap.insert(resolvedPath, inAliasPath);
        return true;
    }

    void UnaliasImagePath(const QString &inSourcePath) override
    {
        m_AliasImageMap.remove(inSourcePath);
    }

    QString GetImagePath(const QString &inSourcePath) override
    {
        TAliasImageMap::iterator theAliasIter = m_AliasImageMap.find(inSourcePath);
        if (theAliasIter != m_AliasImageMap.end())
            return *theAliasIter;
        return inSourcePath;
    }

    template <typename V, typename C>
    void iterateAll(const V &vv, C c)
    {
        for (const auto x : vv)
            c(x);
    }

    void loadTextureImage(SReloadableImageTextureData &data, bool flipCompressed = false)
    {
        QString imagePath = GetImagePath(data.m_path);
        TImageMap::iterator theIter = m_ImageMap.find(imagePath);
        if ((theIter == m_ImageMap.end() || theIter->m_Loaded == false)
                && !imagePath.isEmpty()) {
            NVScopedReleasable<SLoadedTexture> theLoadedImage;
            SImageTextureData textureData;

            doImageLoad(imagePath, theLoadedImage, flipCompressed);

            if (theLoadedImage) {
                textureData = LoadRenderImage(imagePath, *theLoadedImage, data.m_scanTransparency,
                                              data.m_bsdfMipmap);
                data.m_Texture = textureData.m_Texture;
                data.m_TextureFlags = textureData.m_TextureFlags;
                data.m_BSDFMipMap = textureData.m_BSDFMipMap;
                data.m_loaded = true;
                iterateAll(data.m_callbacks, [](SImage *img){ img->m_Flags.SetDirty(true); });
            } else {
                // We want to make sure that bad path fails once and doesn't fail over and over
                // again which could slow down the system quite a bit.
                auto theImage = m_ImageMap.insert(imagePath, SImageEntry());
                theImage->m_Loaded = true;
                qCWarning(WARNING) << "Failed to load image: " << imagePath;
            }
        } else {
            const SImageEntry textureData = *theIter;
            if (textureData.m_Loaded) {
                data.m_Texture = textureData.m_Texture;
                data.m_TextureFlags = textureData.m_TextureFlags;
                data.m_BSDFMipMap = textureData.m_BSDFMipMap;
                data.m_loaded = true;
                iterateAll(data.m_callbacks, [](SImage *img){ img->m_Flags.SetDirty(true); });
            }
        }
    }

    void unloadTextureImage(SReloadableImageTextureData &data)
    {
        CRegisteredString r = m_StrTable->RegisterStr(qPrintable(data.m_path));
        data.m_loaded = false;
        data.m_Texture = nullptr;
        data.m_BSDFMipMap = nullptr;
        data.m_TextureFlags = {};
        iterateAll(data.m_callbacks, [](SImage *img){ img->m_Flags.SetDirty(true); });
        InvalidateBuffer(r);
    }

    void loadSet(const QSet<QString> &imageSet, bool flipCompressed) override
    {
        for (const auto &sourcePath : imageSet) {
            if (!m_reloadableTextures.contains(sourcePath)) {
                auto img = CreateReloadableImage(sourcePath, false, false, flipCompressed);
                if (img != m_nullTexture) {
                    img->m_initialized = false;
                    loadTextureImage(*m_reloadableTextures[sourcePath]);
                }
            } else if (!m_reloadableTextures[sourcePath]->m_loaded) {
                loadTextureImage(*m_reloadableTextures[sourcePath]);
            }
        }
    }

    void unloadSet(const QSet<QString> &imageSet) override
    {
        for (const auto &sourcePath : imageSet) {
            if (m_reloadableTextures.contains(sourcePath)) {
                if (m_reloadableTextures[sourcePath]->m_loaded)
                    unloadTextureImage(*m_reloadableTextures[sourcePath]);
            }
        }
    }

    void reloadAll(bool flipCompressed) override
    {
        for (const auto &tx : qAsConst(m_reloadableTextures)) {
            if (tx->m_loaded) {
                unloadTextureImage(*tx.data());
                loadTextureImage(*tx.data(), flipCompressed);
            }
        }
    }

    virtual ReloadableTexturePtr CreateReloadableImage(const QString &inSourcePath,
                                                       bool inForceScanForTransparency,
                                                       bool inBsdfMipmaps,
                                                       bool flipCompressed) override
    {
        const auto path = inSourcePath;
        const bool inserted = m_reloadableTextures.contains(path);
        if (path.isNull()) {
            if (m_nullTexture.isNull()) {
                m_nullTexture = ReloadableTexturePtr::create();
                m_nullTexture->m_initialized = true;
            }
            return m_nullTexture;
        }
        if (!inserted || (inserted && m_reloadableTextures[path]->m_initialized == false)) {
            if (!inserted)
                m_reloadableTextures.insert(path, ReloadableTexturePtr::create());
            m_reloadableTextures[path]->m_path = path;
            m_reloadableTextures[path]->m_scanTransparency = inForceScanForTransparency;
            m_reloadableTextures[path]->m_bsdfMipmap = inBsdfMipmaps;
            m_reloadableTextures[path]->m_initialized = true;


#ifndef LEGACY_ASTC_LOADING
            if (!m_reloadableResources)
#endif
                loadTextureImage(*m_reloadableTextures[path], flipCompressed);

            QString imagePath = GetImagePath(path);
            TImageMap::iterator theIter = m_ImageMap.find(imagePath);

#ifndef NO_EXTERNAL_LOOKUP
            // Failed to look up image from map, perhaps external of UIP/UIA image
            // Try with full URI and load from filesystem.
            if (theIter == m_ImageMap.end())
                theIter =  m_ImageMap.find(GetImagePath(inSourcePath));
            if (theIter == m_ImageMap.end()) {
                loadTextureImage(*m_reloadableTextures[path], flipCompressed);
                imagePath = GetImagePath(path);
                theIter = m_ImageMap.find(imagePath);
                if (theIter == m_ImageMap.end())
                    theIter =  m_ImageMap.find(GetImagePath(inSourcePath));
            }
#endif
            if (theIter != m_ImageMap.end()) {
                const SImageEntry textureData = *theIter;
                if (textureData.m_Loaded) {
                    m_reloadableTextures[path]->m_Texture = textureData.m_Texture;
                    m_reloadableTextures[path]->m_TextureFlags = textureData.m_TextureFlags;
                    m_reloadableTextures[path]->m_BSDFMipMap = textureData.m_BSDFMipMap;
                    m_reloadableTextures[path]->m_loaded = true;
                }
            }
        }
        return m_reloadableTextures[path];
    }

    void doImageLoad(const QString inImagePath,
                     NVScopedReleasable<SLoadedTexture> &theLoadedImage,
                     bool inFlipCompressed = false)
    {
        QT3DS_PERF_SCOPED_TIMER(m_PerfTimer, "BufferManager: Image Decompression")
        theLoadedImage = SLoadedTexture::Load(
                    inImagePath, m_Context->GetFoundation(), *m_InputStreamFactory,
                    true, inFlipCompressed, m_Context->GetRenderContextType(), this);
        // Hackish solution to custom materials not finding their textures if they are used
        // in sub-presentations.
        if (!theLoadedImage) {
            if (QDir(inImagePath).isRelative()) {
                QString searchPath = inImagePath;

                // Trying relative to search directories.
                if (searchPath.startsWith(QLatin1String("../"))) {
                    auto searchPathRel = searchPath.right(searchPath.length() - 3);
                    theLoadedImage = SLoadedTexture::Load(
                                searchPathRel.toUtf8(), m_Context->GetFoundation(),
                                *m_InputStreamFactory, true, false,
                                m_Context->GetRenderContextType(), this);
                }

                if (!theLoadedImage) {
                    if (searchPath.startsWith(QLatin1String("./")))
                        searchPath.prepend(QLatin1Char('.'));

                    int loops = 0;
                    while (!theLoadedImage && ++loops <= 3) {
                        theLoadedImage = SLoadedTexture::Load(
                                    searchPath.toUtf8(), m_Context->GetFoundation(),
                                    *m_InputStreamFactory, true, false,
                                    m_Context->GetRenderContextType(), this);
                        searchPath.prepend(QLatin1String("../"));
                    }
                }


            } else {
                // Some textures, for example environment maps for custom materials,
                // have absolute path at this point. It points to the wrong place with
                // the new project structure, so we need to split it up and construct
                // the new absolute path here.
                QString wholePath = inImagePath;
                QStringList splitPath = wholePath.split(QLatin1String("../"));
                if (splitPath.size() > 1) {
                    QString searchPath = splitPath.at(0) + splitPath.at(1);
                    int loops = 0;
                    while (!theLoadedImage && ++loops <= 3) {
                        theLoadedImage = SLoadedTexture::Load(
                                    searchPath.toUtf8(), m_Context->GetFoundation(),
                                    *m_InputStreamFactory, true, false,
                                    m_Context->GetRenderContextType(), this);
                        searchPath = splitPath.at(0);
                        for (int i = 0; i < loops; i++)
                            searchPath.append(QLatin1String("../"));
                        searchPath.append(splitPath.at(1));
                    }
                }
            }
        }
    }

    void enableReloadableResources(bool enable) override
    {
        m_reloadableResources = enable;
    }

    bool isReloadableResourcesEnabled() const override
    {
        return m_reloadableResources;
    }

    SImageTextureData LoadRenderImage(const QString &inImagePath,
                                      SLoadedTexture &inLoadedImage,
                                      bool inForceScanForTransparency, bool inBsdfMipmaps) override
    {
        if (!QOpenGLContext::currentContext())
            return SImageTextureData();
        QT3DS_PERF_SCOPED_TIMER(m_PerfTimer, "BufferManager: Image Upload")
        {
            Mutex::ScopedLock __mapLocker(m_LoadedImageSetMutex);
            m_LoadedImageSet.insert(inImagePath);
        }
        bool wasInserted = !m_ImageMap.contains(inImagePath);
        if (wasInserted)
            m_ImageMap.insert(inImagePath, SImageEntry());

        auto &theImage = m_ImageMap[inImagePath];
        theImage.m_Loaded = true;
        // inLoadedImage.EnsureMultiplerOfFour( m_Context->GetFoundation(), inImagePath.c_str() );

        NVRenderTexture2D *theTexture = m_Context->CreateTexture2D();
        if (inLoadedImage.data) {
            qt3ds::render::NVRenderTextureFormats::Enum destFormat = inLoadedImage.format;
            if (inBsdfMipmaps) {
                if (inLoadedImage.format != NVRenderTextureFormats::RGBE8) {
                    if (m_Context->GetRenderContextType() == render::NVRenderContextValues::GLES2)
                        destFormat = qt3ds::render::NVRenderTextureFormats::RGBA8;
                    else
                        destFormat = qt3ds::render::NVRenderTextureFormats::RGBA16F;
                }
            }
            else {
                theTexture->SetTextureData(
                    NVDataRef<QT3DSU8>((QT3DSU8 *)inLoadedImage.data, inLoadedImage.dataSizeInBytes), 0,
                    inLoadedImage.width, inLoadedImage.height, inLoadedImage.format, destFormat);
                {
                    static int enable = qEnvironmentVariableIntValue("QT3DS_GENERATE_MIPMAPS");
                    if (enable) {
                        theTexture->SetMinFilter(NVRenderTextureMinifyingOp::LinearMipmapLinear);
                        theTexture->SetMagFilter(NVRenderTextureMagnifyingOp::Linear);
                        theTexture->GenerateMipmaps();
                    }
                }
            }

            if (inBsdfMipmaps
                && NVRenderTextureFormats::isUncompressedTextureFormat(inLoadedImage.format)) {
                theTexture->SetMinFilter(NVRenderTextureMinifyingOp::LinearMipmapLinear);
                Qt3DSRenderPrefilterTexture *theBSDFMipMap = theImage.m_BSDFMipMap;
                if (theBSDFMipMap == NULL) {
                    theBSDFMipMap = Qt3DSRenderPrefilterTexture::Create(
                        m_Context, inLoadedImage.width, inLoadedImage.height, *theTexture,
                        destFormat, m_Context->GetFoundation());
                    theImage.m_BSDFMipMap = theBSDFMipMap;
                }

                if (theBSDFMipMap) {
                    theBSDFMipMap->Build(inLoadedImage.data, inLoadedImage.dataSizeInBytes,
                                         inLoadedImage.format);
                }
            }
        } else if (inLoadedImage.dds) {
            theImage.m_Texture = theTexture;
            bool supportsCompressedTextures = m_GPUSupportsCompressedTextures;
            bool isACompressedTexture
                    = NVRenderTextureFormats::isCompressedTextureFormat(inLoadedImage.format);
            bool requiresDecompression
                    = (supportsCompressedTextures == false && isACompressedTexture) || false;
            // test code for DXT decompression
            // if ( isDXT ) requiresDecompression = true;
            if (requiresDecompression) {
                qCWarning(WARNING, PERF_INFO)
                    << "Image %s is compressed format which is unsupported by "
                    << "the graphics subsystem, decompressing in CPU"
                    << inImagePath;
            }
            STextureData theDecompressedImage;
            for (int idx = 0; idx < inLoadedImage.dds->numMipmaps; ++idx) {
                if (inLoadedImage.dds->mipwidth[idx] && inLoadedImage.dds->mipheight[idx]) {
                    if (requiresDecompression == false) {
                        theTexture->SetTextureData(
                            toU8DataRef((char *)inLoadedImage.dds->data[idx],
                                        (QT3DSU32)inLoadedImage.dds->size[idx]),
                            (QT3DSU8)idx, (QT3DSU32)inLoadedImage.dds->mipwidth[idx],
                            (QT3DSU32)inLoadedImage.dds->mipheight[idx], inLoadedImage.format);
                    } else {
                        theDecompressedImage =
                            inLoadedImage.DecompressDXTImage(idx, &theDecompressedImage);

                        if (theDecompressedImage.data) {
                            theTexture->SetTextureData(
                                toU8DataRef((char *)theDecompressedImage.data,
                                            (QT3DSU32)theDecompressedImage.dataSizeInBytes),
                                (QT3DSU8)idx, (QT3DSU32)inLoadedImage.dds->mipwidth[idx],
                                (QT3DSU32)inLoadedImage.dds->mipheight[idx],
                                theDecompressedImage.format);
                        }
                    }
                }
            }
            if (theDecompressedImage.data)
                inLoadedImage.ReleaseDecompressedTexture(theDecompressedImage);
        }
        if (wasInserted || inForceScanForTransparency) {
            auto &flags = theImage.m_TextureFlags;
            bool alsoOpaquePixels = false;
            flags.SetHasTransparency(inLoadedImage.ScanForTransparency(alsoOpaquePixels));
            flags.setHasOpaquePixels(alsoOpaquePixels);
            checkAlwaysPremultiplied(inImagePath, flags);
        }
        theImage.m_Texture = theTexture;
        return theImage;
    }

    SImageTextureData LoadRenderImage(const QString &inImagePath,
                                      bool inForceScanForTransparency, bool inBsdfMipmaps) override
    {
        const QString imagePath = GetImagePath(inImagePath);

        if (imagePath.isEmpty())
            return SImageEntry();

        TImageMap::iterator theIter = m_ImageMap.find(inImagePath);
        if (theIter == m_ImageMap.end() && !imagePath.isEmpty()) {
            NVScopedReleasable<SLoadedTexture> theLoadedImage;

            doImageLoad(inImagePath, theLoadedImage);

            if (theLoadedImage) {
                return LoadRenderImage(inImagePath, *theLoadedImage, inForceScanForTransparency,
                                       inBsdfMipmaps);
            } else {
                // We want to make sure that bad path fails once and doesn't fail over and over
                // again
                // which could slow down the system quite a bit.
                auto theImage = m_ImageMap.insert(inImagePath, SImageEntry());
                theImage->m_Loaded = true;
                qCWarning(WARNING) << "Failed to load image: " << inImagePath;
                theIter = theImage;
            }
        }
        return *theIter;
    }

    qt3dsimp::SMultiLoadResult LoadPrimitive(const char8_t *inRelativePath)
    {
        CRegisteredString theName(m_StrTable->RegisterStr(inRelativePath));
        if (m_PrimitiveNames[0].m_PrimitiveName.IsValid() == false) {
            IStringTable &strTable(m_Context->GetStringTable());
            m_PrimitiveNames[0].m_PrimitiveName = strTable.RegisterStr("#Rectangle");
            m_PrimitiveNames[0].m_FileName = strTable.RegisterStr("Rectangle.mesh");
            m_PrimitiveNames[1].m_PrimitiveName = strTable.RegisterStr("#Sphere");
            m_PrimitiveNames[1].m_FileName = strTable.RegisterStr("Sphere.mesh");
            m_PrimitiveNames[2].m_PrimitiveName = strTable.RegisterStr("#Cube");
            m_PrimitiveNames[2].m_FileName = strTable.RegisterStr("Cube.mesh");
            m_PrimitiveNames[3].m_PrimitiveName = strTable.RegisterStr("#Cone");
            m_PrimitiveNames[3].m_FileName = strTable.RegisterStr("Cone.mesh");
            m_PrimitiveNames[4].m_PrimitiveName = strTable.RegisterStr("#Cylinder");
            m_PrimitiveNames[4].m_FileName = strTable.RegisterStr("Cylinder.mesh");
        }
        for (size_t idx = 0; idx < 5; ++idx) {
            if (m_PrimitiveNames[idx].m_PrimitiveName == theName) {
                CFileTools::CombineBaseAndRelative(GetPrimitivesDirectory(),
                                                   m_PrimitiveNames[idx].m_FileName, m_PathBuilder);
                QT3DSU32 id = 1;
                NVScopedRefCounted<IRefCountedInputStream> theInStream(
                    m_InputStreamFactory->GetStreamForFile(m_PathBuilder.c_str()));
                if (theInStream)
                    return qt3dsimp::Mesh::LoadMulti(m_Context->GetAllocator(), *theInStream, id);
                else {
                    qCCritical(INTERNAL_ERROR, "Unable to find mesh primitive %s",
                        m_PathBuilder.c_str());
                    return qt3dsimp::SMultiLoadResult();
                }
            }
        }
        return qt3dsimp::SMultiLoadResult();
    }

    virtual NVConstDataRef<QT3DSU8> CreatePackedPositionDataArray(
            const qt3dsimp::SMultiLoadResult &inResult)
    {
        // we assume a position consists of 3 floats
        QT3DSU32 vertexCount = inResult.m_Mesh->m_VertexBuffer.m_Data.size()
            / inResult.m_Mesh->m_VertexBuffer.m_Stride;
        QT3DSU32 dataSize = vertexCount * 3 * sizeof(QT3DSF32);
        QT3DSF32 *posData = static_cast<QT3DSF32 *>(
                    QT3DS_ALLOC(m_Context->GetAllocator(), dataSize,
                                "SRenderMesh::CreatePackedPositionDataArray"));
        QT3DSU8 *baseOffset = reinterpret_cast<QT3DSU8 *>(inResult.m_Mesh);
        // copy position data
        if (posData) {
            QT3DSF32 *srcData = reinterpret_cast<QT3DSF32 *>(
                        inResult.m_Mesh->m_VertexBuffer.m_Data.begin(baseOffset));
            QT3DSU32 srcStride = inResult.m_Mesh->m_VertexBuffer.m_Stride / sizeof(QT3DSF32);
            QT3DSF32 *dstData = posData;
            QT3DSU32 dstStride = 3;

            for (QT3DSU32 i = 0; i < vertexCount; ++i) {
                dstData[0] = srcData[0];
                dstData[1] = srcData[1];
                dstData[2] = srcData[2];

                dstData += dstStride;
                srcData += srcStride;
            }

            return toConstDataRef(reinterpret_cast<const qt3ds::QT3DSU8 *>(posData), dataSize);
        }

        return NVConstDataRef<QT3DSU8>();
    }

    SRenderMesh *createRenderMesh(const qt3dsimp::SMultiLoadResult &result)
    {
        SRenderMesh *theNewMesh = QT3DS_NEW(m_Context->GetAllocator(), SRenderMesh)(
            qt3ds::render::NVRenderDrawMode::Triangles,
            qt3ds::render::NVRenderWinding::CounterClockwise, result.m_Id,
            m_Context->GetAllocator());
        QT3DSU8 *baseAddress = reinterpret_cast<QT3DSU8 *>(result.m_Mesh);
        NVConstDataRef<QT3DSU8> theVBufData(
            result.m_Mesh->m_VertexBuffer.m_Data.begin(baseAddress),
            result.m_Mesh->m_VertexBuffer.m_Data.size());

        NVRenderVertexBuffer *theVertexBuffer = m_Context->CreateVertexBuffer(
            qt3ds::render::NVRenderBufferUsageType::Static,
            result.m_Mesh->m_VertexBuffer.m_Data.m_Size,
            result.m_Mesh->m_VertexBuffer.m_Stride, theVBufData);

        // create a tight packed position data VBO
        // this should improve our depth pre pass rendering
        NVRenderVertexBuffer *thePosVertexBuffer = nullptr;
        NVConstDataRef<QT3DSU8> posData = CreatePackedPositionDataArray(result);
        if (posData.size()) {
            thePosVertexBuffer
                = m_Context->CreateVertexBuffer(qt3ds::render::NVRenderBufferUsageType::Static,
                                                posData.size(), 3 * sizeof(QT3DSF32), posData);
        }

        NVRenderIndexBuffer *theIndexBuffer = nullptr;
        if (result.m_Mesh->m_IndexBuffer.m_Data.size()) {
            using qt3ds::render::NVRenderComponentTypes;
            QT3DSU32 theIndexBufferSize = result.m_Mesh->m_IndexBuffer.m_Data.size();
            NVRenderComponentTypes::Enum bufComponentType =
                result.m_Mesh->m_IndexBuffer.m_ComponentType;
            QT3DSU32 sizeofType
                = qt3ds::render::NVRenderComponentTypes::getSizeofType(bufComponentType);

            if (sizeofType == 2 || sizeofType == 4) {
                // Ensure type is unsigned; else things will fail in rendering pipeline.
                if (bufComponentType == NVRenderComponentTypes::QT3DSI16)
                    bufComponentType = NVRenderComponentTypes::QT3DSU16;
                if (bufComponentType == NVRenderComponentTypes::QT3DSI32)
                    bufComponentType = NVRenderComponentTypes::QT3DSU32;

                NVConstDataRef<QT3DSU8> theIBufData(
                    result.m_Mesh->m_IndexBuffer.m_Data.begin(baseAddress),
                    result.m_Mesh->m_IndexBuffer.m_Data.size());
                theIndexBuffer = m_Context->CreateIndexBuffer(
                    qt3ds::render::NVRenderBufferUsageType::Static, bufComponentType,
                    theIndexBufferSize, theIBufData);
            } else {
                QT3DS_ASSERT(false);
            }
        }
        nvvector<qt3ds::render::NVRenderVertexBufferEntry> &theEntryBuffer(m_EntryBuffer);
        theEntryBuffer.resize(result.m_Mesh->m_VertexBuffer.m_Entries.size());
        for (QT3DSU32 entryIdx = 0,
             entryEnd = result.m_Mesh->m_VertexBuffer.m_Entries.size();
             entryIdx < entryEnd; ++entryIdx) {
            theEntryBuffer[entryIdx]
                = result.m_Mesh->m_VertexBuffer.m_Entries.index(baseAddress, entryIdx)
                    .ToVertexBufferEntry(baseAddress);
        }
        // create our attribute layout
        NVRenderAttribLayout *theAttribLayout
            = m_Context->CreateAttributeLayout(theEntryBuffer);
        // create our attribute layout for depth pass
        qt3ds::render::NVRenderVertexBufferEntry theEntries[] = {
            qt3ds::render::NVRenderVertexBufferEntry(
                "attr_pos", qt3ds::render::NVRenderComponentTypes::QT3DSF32, 3),
        };
        NVRenderAttribLayout *theAttribLayoutDepth
            = m_Context->CreateAttributeLayout(toConstDataRef(theEntries, 1));

        // create input assembler object
        QT3DSU32 strides = result.m_Mesh->m_VertexBuffer.m_Stride;
        QT3DSU32 offsets = 0;
        NVRenderInputAssembler *theInputAssembler = m_Context->CreateInputAssembler(
            theAttribLayout, toConstDataRef(&theVertexBuffer, 1), theIndexBuffer,
            toConstDataRef(&strides, 1), toConstDataRef(&offsets, 1),
            result.m_Mesh->m_DrawMode);

        // create depth input assembler object
        QT3DSU32 posStrides = thePosVertexBuffer ? 3 * sizeof(QT3DSF32) : strides;
        NVRenderInputAssembler *theInputAssemblerDepth = m_Context->CreateInputAssembler(
            theAttribLayoutDepth,
            toConstDataRef(thePosVertexBuffer ? &thePosVertexBuffer : &theVertexBuffer, 1),
                    theIndexBuffer, toConstDataRef(&posStrides, 1), toConstDataRef(&offsets, 1),
            result.m_Mesh->m_DrawMode);

        NVRenderInputAssembler *theInputAssemblerPoints = m_Context->CreateInputAssembler(
            theAttribLayoutDepth,
            toConstDataRef(thePosVertexBuffer ? &thePosVertexBuffer : &theVertexBuffer, 1),
                    nullptr, toConstDataRef(&posStrides, 1), toConstDataRef(&offsets, 1),
            NVRenderDrawMode::Points);

        if (!theInputAssembler || !theInputAssemblerDepth || !theInputAssemblerPoints) {
            QT3DS_ASSERT(false);
            return nullptr;
        }
        theNewMesh->m_Joints.resize(result.m_Mesh->m_Joints.size());
        for (QT3DSU32 jointIdx = 0, jointEnd = result.m_Mesh->m_Joints.size();
             jointIdx < jointEnd; ++jointIdx) {
            const qt3dsimp::Joint &theImportJoint(
                result.m_Mesh->m_Joints.index(baseAddress, jointIdx));
            SRenderJoint &theNewJoint(theNewMesh->m_Joints[jointIdx]);
            theNewJoint.m_JointID = theImportJoint.m_JointID;
            theNewJoint.m_ParentID = theImportJoint.m_ParentID;
            memCopy(theNewJoint.m_invBindPose, theImportJoint.m_invBindPose,
                    16 * sizeof(QT3DSF32));
            memCopy(theNewJoint.m_localToGlobalBoneSpace,
                    theImportJoint.m_localToGlobalBoneSpace, 16 * sizeof(QT3DSF32));
        }

        for (QT3DSU32 subsetIdx = 0, subsetEnd = result.m_Mesh->m_Subsets.size();
             subsetIdx < subsetEnd; ++subsetIdx) {
            SRenderSubset theSubset(m_Context->GetAllocator());
            const qt3dsimp::MeshSubset &source(
                result.m_Mesh->m_Subsets.index(baseAddress, subsetIdx));
            theSubset.m_Bounds = source.m_Bounds;
            theSubset.m_Count = source.m_Count;
            theSubset.m_Offset = source.m_Offset;
            theSubset.m_Joints = theNewMesh->m_Joints;
            theSubset.m_Name = m_StrTable->RegisterStr(source.m_Name.begin(baseAddress));
            theVertexBuffer->addRef();
            theSubset.m_VertexBuffer = theVertexBuffer;
            if (thePosVertexBuffer) {
                thePosVertexBuffer->addRef();
                theSubset.m_PosVertexBuffer = thePosVertexBuffer;
            }
            if (theIndexBuffer) {
                theIndexBuffer->addRef();
                theSubset.m_IndexBuffer = theIndexBuffer;
            }
            theSubset.m_InputAssembler = theInputAssembler;
            theSubset.m_InputAssemblerDepth = theInputAssemblerDepth;
            theSubset.m_InputAssemblerPoints = theInputAssemblerPoints;
            theSubset.m_PrimitiveType = result.m_Mesh->m_DrawMode;
            theInputAssembler->addRef();
            theInputAssemblerDepth->addRef();
            theSubset.m_InputAssemblerPoints->addRef();
            theNewMesh->m_Subsets.push_back(theSubset);
        }
        // If we want to, we can break up models into sub-subsets.
        // These are assumed to use the same material as the outer subset but have fewer triangles
        // and should have a more exact bounding box. This sort of thing helps with using the
        // frustum culling system but it is really done incorrectly.
        // It should be done via some sort of oct-tree mechanism and so that the sub-subsets
        // are spatially sorted and it should only be done upon save-to-binary with the
        // results saved out to disk. As you can see, doing it properly requires some real
        // engineering effort so it is somewhat unlikely it will ever happen.
        // Or it could be done on import if someone really wants to change the mesh buffer
        // format. Either way it isn't going to happen here and it isn't going to happen this way
        // but this is a working example of using the technique.
#ifdef QT3DS_RENDER_GENERATE_SUB_SUBSETS
        Option<qt3ds::render::NVRenderVertexBufferEntry> thePosAttrOpt
            = theVertexBuffer->GetEntryByName("attr_pos");
        bool hasPosAttr = thePosAttrOpt.hasValue()
            && thePosAttrOpt->m_ComponentType == qt3ds::render::NVRenderComponentTypes::QT3DSF32
            && thePosAttrOpt->m_NumComponents == 3;

        for (size_t subsetIdx = 0, subsetEnd = theNewMesh->m_Subsets.size();
             subsetIdx < subsetEnd; ++subsetIdx) {
            SRenderSubset &theOuterSubset = theNewMesh->m_Subsets[subsetIdx];
            if (theOuterSubset.m_Count && theIndexBuffer
                && theIndexBuffer->GetComponentType()
                    == qt3ds::render::NVRenderComponentTypes::QT3DSU16
                && theNewMesh->m_DrawMode == NVRenderDrawMode::Triangles && hasPosAttr) {
                // Num tris in a sub subset.
                QT3DSU32 theSubsetSize = 3334 * 3; // divisible by three.
                size_t theNumSubSubsets = ((theOuterSubset.m_Count - 1) / theSubsetSize) + 1;
                QT3DSU32 thePosAttrOffset = thePosAttrOpt->m_FirstItemOffset;
                const QT3DSU8 *theVertData = result.m_Mesh->m_VertexBuffer.m_Data.begin();
                const QT3DSU8 *theIdxData = result.m_Mesh->m_IndexBuffer.m_Data.begin();
                QT3DSU32 theVertStride = result.m_Mesh->m_VertexBuffer.m_Stride;
                QT3DSU32 theOffset = theOuterSubset.m_Offset;
                QT3DSU32 theCount = theOuterSubset.m_Count;
                for (size_t subSubsetIdx = 0, subSubsetEnd = theNumSubSubsets;
                     subSubsetIdx < subSubsetEnd; ++subSubsetIdx) {
                    SRenderSubsetBase theBase;
                    theBase.m_Offset = theOffset;
                    theBase.m_Count = NVMin(theSubsetSize, theCount);
                    theBase.m_Bounds.setEmpty();
                    theCount -= theBase.m_Count;
                    theOffset += theBase.m_Count;
                    // Create new bounds.
                    // Offset is in item size, not bytes.
                    const QT3DSU16 *theSubsetIdxData
                        = reinterpret_cast<const QT3DSU16 *>(theIdxData + theBase.m_Offset * 2);
                    for (size_t theIdxIdx = 0, theIdxEnd = theBase.m_Count;
                         theIdxIdx < theIdxEnd; ++theIdxIdx) {
                        QT3DSU32 theVertOffset = theSubsetIdxData[theIdxIdx] * theVertStride;
                        theVertOffset += thePosAttrOffset;
                        QT3DSVec3 thePos = *(
                            reinterpret_cast<const QT3DSVec3 *>(theVertData + theVertOffset));
                        theBase.m_Bounds.include(thePos);
                    }
                    theOuterSubset.m_SubSubsets.push_back(theBase);
                }
            } else {
                SRenderSubsetBase theBase;
                theBase.m_Bounds = theOuterSubset.m_Bounds;
                theBase.m_Count = theOuterSubset.m_Count;
                theBase.m_Offset = theOuterSubset.m_Offset;
                theOuterSubset.m_SubSubsets.push_back(theBase);
            }
        }
#endif
        if (posData.size()) {
            m_Context->GetAllocator().deallocate(
                        static_cast<void *>(const_cast<qt3ds::QT3DSU8 *>(posData.begin())));
        }

        return theNewMesh;
    }

    void loadCustomMesh(const QString &name, qt3dsimp::Mesh *mesh) override
    {
        if (!name.isEmpty() && mesh) {
            CRegisteredString meshName = m_StrTable->RegisterStr(name);
            pair<TMeshMap::iterator, bool> theMesh
                = m_MeshMap.insert({ meshName, static_cast<SRenderMesh *>(nullptr) });
            // Only create the mesh if it doesn't yet exist
            if (theMesh.second) {
                qt3dsimp::SMultiLoadResult result;
                result.m_Mesh = mesh;
                theMesh.first->second = createRenderMesh(result);
            }
        }
    }

    SRenderMesh *LoadMesh(CRegisteredString inMeshPath) override
    {
        if (!QOpenGLContext::currentContext())
            return nullptr;
        if (inMeshPath.IsValid() == false)
            return nullptr;
        pair<TMeshMap::iterator, bool> theMesh =
            m_MeshMap.insert(make_pair(inMeshPath, static_cast<SRenderMesh *>(nullptr)));
        if (theMesh.second) {
            // Check to see if this is primitive
            qt3dsimp::SMultiLoadResult theResult = LoadPrimitive(inMeshPath);

            // Attempt a load from the filesystem if this mesh isn't a primitive.
            if (!theResult.m_Mesh) {
                m_PathBuilder = inMeshPath;
                TStr::size_type pound = m_PathBuilder.rfind('#');
                QT3DSU32 id = 0;
                if (pound != TStr::npos) {
                    id = QT3DSU32(atoi(m_PathBuilder.c_str() + pound + 1));
                    m_PathBuilder.erase(m_PathBuilder.begin() + pound, m_PathBuilder.end());
                }
                NVScopedRefCounted<IRefCountedInputStream> theStream(
                    m_InputStreamFactory->GetStreamForFile(m_PathBuilder.c_str()));
                if (theStream) {
                    theResult = qt3dsimp::Mesh::LoadMulti(
                                m_Context->GetAllocator(), *theStream, id);
                }
                if (!theResult.m_Mesh)
                    qCWarning(WARNING, "Failed to load mesh: %s", m_PathBuilder.c_str());
            }

            if (theResult.m_Mesh) {
                theMesh.first->second = createRenderMesh(theResult);
                m_Context->GetAllocator().deallocate(theResult.m_Mesh);
            }
        }
        return theMesh.first->second;
    }

    SRenderMesh *CreateMesh(Qt3DSBCharPtr inSourcePath, QT3DSU8 *inVertData, QT3DSU32 inNumVerts,
                            QT3DSU32 inVertStride, QT3DSU32 *inIndexData, QT3DSU32 inIndexCount,
                            qt3ds::NVBounds3 inBounds) override
    {
        CRegisteredString sourcePath = m_StrTable->RegisterStr(inSourcePath);

        // eastl::pair<CRegisteredString, SRenderMesh*> thePair(sourcePath, (SRenderMesh*)NULL);
        pair<TMeshMap::iterator, bool> theMesh;
        // Make sure there isn't already a buffer entry for this mesh.
        if (m_MeshMap.contains(sourcePath)) {
            theMesh = make_pair<TMeshMap::iterator, bool>(m_MeshMap.find(sourcePath), true);
        } else {
            theMesh = m_MeshMap.insert(make_pair(sourcePath, (SRenderMesh *)NULL));
        }

        if (theMesh.second == true) {
            SRenderMesh *theNewMesh = QT3DS_NEW(m_Context->GetAllocator(), SRenderMesh)(
                qt3ds::render::NVRenderDrawMode::Triangles,
                qt3ds::render::NVRenderWinding::CounterClockwise, 0, m_Context->GetAllocator());

            // If we failed to create the RenderMesh, return a failure.
            if (!theNewMesh) {
                QT3DS_ASSERT(false);
                return NULL;
            }

            // Get rid of any old mesh that was sitting here and fill it with a new one.
            // NOTE : This is assuming that the source of our mesh data doesn't do its own memory
            // management and always returns new buffer pointers every time.
            // Don't know for sure if that's what we'll get from our intended sources, but that's
            // easily
            // adjustable by looking for matching pointers in the Subsets.
            if (theNewMesh && theMesh.first->second != NULL) {
                delete theMesh.first->second;
                theMesh.first->second = NULL;
            }

            theMesh.first->second = theNewMesh;
            QT3DSU32 vertDataSize = inNumVerts * inVertStride;
            NVConstDataRef<QT3DSU8> theVBufData(inVertData, vertDataSize);
            // NVConstDataRef<QT3DSU8> theVBufData( theResult.m_Mesh->m_VertexBuffer.m_Data.begin(
            // baseAddress )
            //		, theResult.m_Mesh->m_VertexBuffer.m_Data.size() );

            NVRenderVertexBuffer *theVertexBuffer =
                m_Context->CreateVertexBuffer(qt3ds::render::NVRenderBufferUsageType::Static,
                                              vertDataSize, inVertStride, theVBufData);
            NVRenderIndexBuffer *theIndexBuffer = NULL;
            if (inIndexData != NULL && inIndexCount > 3) {
                NVConstDataRef<QT3DSU8> theIBufData((QT3DSU8 *)inIndexData, inIndexCount * sizeof(QT3DSU32));
                theIndexBuffer =
                    m_Context->CreateIndexBuffer(qt3ds::render::NVRenderBufferUsageType::Static,
                                                 qt3ds::render::NVRenderComponentTypes::QT3DSU32,
                                                 inIndexCount * sizeof(QT3DSU32), theIBufData);
            }

            // WARNING
            // Making an assumption here about the contents of the stream
            // PKC TODO : We may have to consider some other format.
            qt3ds::render::NVRenderVertexBufferEntry theEntries[] = {
                qt3ds::render::NVRenderVertexBufferEntry("attr_pos",
                                                      qt3ds::render::NVRenderComponentTypes::QT3DSF32, 3),
                qt3ds::render::NVRenderVertexBufferEntry(
                    "attr_uv", qt3ds::render::NVRenderComponentTypes::QT3DSF32, 2, 12),
                qt3ds::render::NVRenderVertexBufferEntry(
                    "attr_norm", qt3ds::render::NVRenderComponentTypes::QT3DSF32, 3, 18),
            };

            // create our attribute layout
            NVRenderAttribLayout *theAttribLayout =
                m_Context->CreateAttributeLayout(toConstDataRef(theEntries, 3));
            /*
            // create our attribute layout for depth pass
            qt3ds::render::NVRenderVertexBufferEntry theEntriesDepth[] = {
                    qt3ds::render::NVRenderVertexBufferEntry( "attr_pos",
            qt3ds::render::NVRenderComponentTypes::QT3DSF32, 3 ),
            };
            NVRenderAttribLayout* theAttribLayoutDepth = m_Context->CreateAttributeLayout(
            toConstDataRef( theEntriesDepth, 1 ) );
            */
            // create input assembler object
            QT3DSU32 strides = inVertStride;
            QT3DSU32 offsets = 0;
            NVRenderInputAssembler *theInputAssembler = m_Context->CreateInputAssembler(
                theAttribLayout, toConstDataRef(&theVertexBuffer, 1), theIndexBuffer,
                toConstDataRef(&strides, 1), toConstDataRef(&offsets, 1),
                qt3ds::render::NVRenderDrawMode::Triangles);

            if (!theInputAssembler) {
                QT3DS_ASSERT(false);
                return NULL;
            }

            // Pull out just the mesh object name from the total path
            eastl::string fullName(inSourcePath);
            eastl::string subName(inSourcePath);
            if (fullName.rfind("#") != eastl::string::npos) {
                subName = fullName.substr(fullName.rfind("#"), eastl::string::npos);
            }

            theNewMesh->m_Joints.clear();
            SRenderSubset theSubset(m_Context->GetAllocator());
            theSubset.m_Bounds = inBounds;
            theSubset.m_Count = inIndexCount;
            theSubset.m_Offset = 0;
            theSubset.m_Joints = theNewMesh->m_Joints;
            theSubset.m_Name = m_StrTable->RegisterStr(subName.c_str());
            theVertexBuffer->addRef();
            theSubset.m_VertexBuffer = theVertexBuffer;
            theSubset.m_PosVertexBuffer = NULL;
            if (theIndexBuffer)
                theIndexBuffer->addRef();
            theSubset.m_IndexBuffer = theIndexBuffer;
            theSubset.m_InputAssembler = theInputAssembler;
            theSubset.m_InputAssemblerDepth = theInputAssembler;
            theSubset.m_InputAssemblerPoints = theInputAssembler;
            theSubset.m_PrimitiveType = qt3ds::render::NVRenderDrawMode::Triangles;
            theSubset.m_InputAssembler->addRef();
            theSubset.m_InputAssemblerDepth->addRef();
            theSubset.m_InputAssemblerPoints->addRef();
            theNewMesh->m_Subsets.push_back(theSubset);
        }

        return theMesh.first->second;
    }

    void addImageProvider(const QString &providerId, QQmlImageProviderBase *provider) override
    {
        QString providerIdLower = providerId.toLower();
        QSharedPointer<QQmlImageProviderBase> sp(provider);
        m_imageProviders.insert(std::move(providerIdLower), std::move(sp));
    }

    QQmlImageProviderBase *imageProvider(const QString &providerId) override
    {
        const QString providerIdLower = providerId.toLower();
        return m_imageProviders.value(providerIdLower).data();
    }

    void ReleaseMesh(SRenderMesh &inMesh)
    {
        for (QT3DSU32 subsetIdx = 0, subsetEnd = inMesh.m_Subsets.size(); subsetIdx < subsetEnd;
             ++subsetIdx) {
            inMesh.m_Subsets[subsetIdx].m_VertexBuffer->release();
            if (inMesh.m_Subsets[subsetIdx].m_PosVertexBuffer) // can be NULL
                inMesh.m_Subsets[subsetIdx].m_PosVertexBuffer->release();
            if (inMesh.m_Subsets[subsetIdx].m_IndexBuffer) // can be NULL
                inMesh.m_Subsets[subsetIdx].m_IndexBuffer->release();
            inMesh.m_Subsets[subsetIdx].m_InputAssembler->release();
            inMesh.m_Subsets[subsetIdx].m_InputAssemblerDepth->release();
            if (inMesh.m_Subsets[subsetIdx].m_InputAssemblerPoints)
                inMesh.m_Subsets[subsetIdx].m_InputAssemblerPoints->release();
        }
        NVDelete(m_Context->GetAllocator(), &inMesh);
    }
    void ReleaseTexture(SImageEntry &inEntry)
    {
        if (inEntry.m_Texture)
            inEntry.m_Texture->release();
        if (inEntry.m_BSDFMipMap)
            inEntry.m_BSDFMipMap->release();
    }
    void Clear() override
    {
        m_reloadableTextures.clear();
        for (TMeshMap::iterator iter = m_MeshMap.begin(), end = m_MeshMap.end(); iter != end;
             ++iter) {
            SRenderMesh *theMesh = iter->second;
            if (theMesh)
                ReleaseMesh(*theMesh);
        }
        m_MeshMap.clear();
        for (TImageMap::iterator iter = m_ImageMap.begin(), end = m_ImageMap.end(); iter != end;
             ++iter) {
            SImageEntry &theEntry = *iter;
            ReleaseTexture(theEntry);
        }
        m_ImageMap.clear();
        m_AliasImageMap.clear();
        {
            Mutex::ScopedLock __locker(m_LoadedImageSetMutex);
            m_LoadedImageSet.clear();
        }
        m_imageProviders.clear();
    }
    void InvalidateBuffer(CRegisteredString inSourcePath) override
    {
        {
            TMeshMap::iterator iter = m_MeshMap.find(inSourcePath);
            if (iter != m_MeshMap.end()) {
                if (iter->second)
                    ReleaseMesh(*iter->second);
                m_MeshMap.erase(iter);
                return;
            }
        }
        {
            const QString imagePath = QString::fromLatin1(inSourcePath.c_str());
            TImageMap::iterator iter = m_ImageMap.find(imagePath);
            if (iter != m_ImageMap.end()) {
                SImageEntry &theEntry = *iter;
                ReleaseTexture(theEntry);
                m_ImageMap.remove(imagePath);
                {
                    Mutex::ScopedLock __locker(m_LoadedImageSetMutex);
                    m_LoadedImageSet.remove(imagePath);
                }
            }
        }
    }
    IStringTable &GetStringTable() override { return *m_StrTable; }
};
}

IBufferManager &IBufferManager::Create(NVRenderContext &inRenderContext, IStringTable &inStrTable,
                                       IInputStreamFactory &inFactory, IPerfTimer &inPerfTimer)
{
    return *QT3DS_NEW(inRenderContext.GetAllocator(), SBufferManager)(inRenderContext, inStrTable,
                                                                      inFactory, inPerfTimer);
}
